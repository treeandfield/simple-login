<?php

//ERROR
ini_set('display_errors', '1');
error_reporting(-1);

//TIME
date_default_timezone_set('America/Los_Angeles');


/* ALL REQUEST SHOULD BE A JSON OBJECT

{
	"id":"345345453453543534",
	"action":"read",
	"database":"users",
	"doc":{
		"_id":"dsfdsfffssfdfd",
		"_rev":"32534244334534543",
		"some":"info",
		"maybe":"here"
	}
}

$JSON = stripslashes($_REQUEST['data']);
$JSONObj = json_decode($JSON );

$action = $JSONObj->action;
$docId =  $JSONObj->id;
$db =  $JSONObj->database;

$dataObj = $JSONObj->doc;


*/

//what to do
$action = $_REQUEST['action'];

$docId = $_REQUEST['docId'];
$data = stripslashes($_REQUEST['data']);
$dataObj = json_decode($data);


// require sag JS
require_once('sag-0.9.0/src/Sag.php');


if($action){
	
	//login to cloudant
	$sag = new Sag('indigoshade.cloudant.com');
	
	// Have the json object conatin the database name
	$sag->setDatabase('users');
	
	
	$sag->setHTTPAdapter('HTTP_NATIVE_SOCKETS');
	$result = $sag->login('indigoshade', '3zDuz1td@t@b@s3');
	
	
	// what are we going to do
	switch ($action) {
	
		// return doc --------------------------------------------
		case 'read':
			$doc = $sag->get($docId)->body;
			print json_encode($doc);
			break;
			
		
		// return doc --------------------------------------------
		case 'emailSearch':
	
			$path = "_design/users/_search/emailSearch?q=email:".$dataObj->email."&include_docs=true";
			$doc = $sag->get($path)->body;
			print json_encode($doc);
			break;
			
		
	
		// add doc ----------------------------------------------
		case 'add':
			$add = $sag->post($data);
			print json_encode($add->body);
			break;
			
		
		//edit doc ----------------------------------------------
		case 'edit':
			
			if(!$sag->put($dataObj->_id, $dataObj)->body->ok) {
				print '{"status":"error"}';
			}
			else{
				//test results
				$savedoc = $sag->get($docId)->body;
				print json_encode($savedoc);
			}
				
			break;
		
		//delete doc ----------------------------------------------
		case 'delete':
			$doc = $sag->get($docId)->body;
			$rev = $doc->_rev;
			
			$sag->delete($docId, $rev);
			
			print '{"status":"deleted"}';
			
			break;
						
					
		//default message --------------------------------------------
		default:
			print '{"status":"WTF"]';
			break;
	
	}

}

else{
	print '{"status":"no action"]';
}

?>
